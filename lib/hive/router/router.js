var kind = require('enyo/kind');
var Control = require('enyo/Control');

// Panel stuff
var Panels = require('layout/Panels');
var CardArranger = require('layout/CardArranger');

module.exports = kind({
    name: 'Panels',
    kind: Panels,
    fit: true,
    realtimeFit: true,
    narrowFit: true,
    arrangerKind: CardArranger,
    components: [
        { content: "true", style: "background: blue;" },
        { content: "false", style: "background: red;"}
    ]
});