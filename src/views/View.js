var kind = require('enyo/kind');
var Control = require('enyo/Control');

// onyx stuff
var toolbar = require('onyx/Toolbar'),
    Button = require('enyo/Button');

// Layout stuff
var FittableRows = require('layout/FittableRows'),
    FittableColumns = require('layout/FittableColumns');

var router = require('hive/router');

module.exports = kind({
    name: 'MainView',
    kind: FittableRows,
    classes: 'enyo-fit',
    components: [
        { kind: router }
    ]
});