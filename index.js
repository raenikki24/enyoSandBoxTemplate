// This is the default "main" file, specified from the root package.json file
// The ready function is excuted when the DOM is ready for usage.

/* This is how we import classes from enyo library CommonJS Style */
var ready = require('enyo/ready');
var kind = require('enyo/kind');
var control = require('enyo/Control');

var View = require('./src/views/View');
var View2 = require('./src/views/View2');
var View3 = require('./src/views/View3');
var View4 = require('./src/views/View4.js');

var App = control.kind({
    classes: 'enyo-fit',
    components: [
        {
            kind: View
        }
    ],
    rendered: function(){
        this.inherited(arguments);
    }
});

ready(function() {
    /* As usual App is a kind class, instantiate it and use the enyo function
       "renderInto the document body of the html page" */
    new App().renderInto(document.body);
});
